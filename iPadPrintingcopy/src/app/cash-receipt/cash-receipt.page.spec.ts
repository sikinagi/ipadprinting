import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashReceiptPage } from './cash-receipt.page';

describe('CashReceiptPage', () => {
  let component: CashReceiptPage;
  let fixture: ComponentFixture<CashReceiptPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashReceiptPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashReceiptPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
