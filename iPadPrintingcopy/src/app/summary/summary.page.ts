import { Component, OnInit } from '@angular/core';
import {SignaturePage} from '../signature/signature.page';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.page.html',
  styleUrls: ['./summary.page.scss'],
})
export class SummaryPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
