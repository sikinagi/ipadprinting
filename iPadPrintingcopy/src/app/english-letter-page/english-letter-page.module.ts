import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EnglishLetterPagePage } from './english-letter-page.page';

const routes: Routes = [
  {
    path: '',
    component: EnglishLetterPagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EnglishLetterPagePage]
})
export class EnglishLetterPagePageModule {}
