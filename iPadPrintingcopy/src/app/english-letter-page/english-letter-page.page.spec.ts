import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnglishLetterPagePage } from './english-letter-page.page';

describe('EnglishLetterPagePage', () => {
  let component: EnglishLetterPagePage;
  let fixture: ComponentFixture<EnglishLetterPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnglishLetterPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnglishLetterPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
