import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { HttpClientModule } from '@angular/common/http';
import { NgStyle } from '@angular/common';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {Library, library} from '@fortawesome/fontawesome-svg-core';
import {faCoffee} from '@fortawesome/free-solid-svg-icons';
import {faSearch} from '@fortawesome/free-solid-svg-icons';
import {faFilePowerpoint} from '@fortawesome/free-solid-svg-icons';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import {faEllipsisH} from '@fortawesome/free-solid-svg-icons';
// import { BackgroundMode } from '@ionic-native/background-mode/ngx';

// library.add(faCoffee, faSearch, faFilePowerpoint);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, SignaturePadModule, HttpClientModule, FontAwesomeModule],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    // BackgroundMode,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  otp: any;
  empDetails2: any;
  requestDetails: any;
  empDetails: any;
  prepaymentData: any;
  prepaymentList: any;
  invoiceId: any;
  invoiceNumber: any;
  referenceNum: any;
  static printOptions: any = [];
  static personDetails: any = [];
  static letterType: any;
  static appName: string;

  static prepaymentDetails: any = [];
static receiptInvoiceList: any = [{}];

  blankPage: any = true;
  constructor()
  {
    this.prepaymentData = 'x1';
    AppModule.printOptions=[{prompt:'x',methodName:'x'}];
    AppModule.personDetails = [];
    AppModule.letterType = 'x1';
 AppModule.printOptions[0].prompt='prompt 1';
 this.blankPage=true;
 AppModule.appName='NA';
 AppModule.prepaymentDetails=[];
AppModule.receiptInvoiceList = [{}];

  }
}
