import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AppModule} from '../app.module';

@Component({
  selector: 'app-my-apps',
  templateUrl: './my-apps.page.html',
  styleUrls: ['./my-apps.page.scss'],
})
export class MyAppsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  openApps(x)
  {
     AppModule.appName=x;
      this.router.navigateByUrl('/service-page');    
  }

}
