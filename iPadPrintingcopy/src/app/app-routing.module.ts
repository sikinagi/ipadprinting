import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'summary', loadChildren: './summary/summary.module#SummaryPageModule' },
  { path: 'signature', loadChildren: './signature/signature.module#SignaturePageModule' },
  { path: 'list-pre-payments', loadChildren: './list-pre-payments/list-pre-payments.module#ListPrePaymentsPageModule' },
  { path: 'service-page', loadChildren: './service-page/service-page.module#ServicePagePageModule' },
  { path: 'action-page', loadChildren: './action-page/action-page.module#ActionPagePageModule' },
  { path: 'tecom-letter-page', loadChildren: './tecom-letter-page/tecom-letter-page.module#TecomLetterPagePageModule' },
  { path: 'english-letter-page', loadChildren: './english-letter-page/english-letter-page.module#EnglishLetterPagePageModule' },
//  { path: 'arabiic-letter-page', loadChildren: './arabiic-letter-page/arabiic-letter-page.module#ArabiicLetterPagePageModule' },
  { path: 'arabic-letter-page', loadChildren: './arabic-letter-page/arabic-letter-page.module#ArabicLetterPagePageModule' },
  { path: 'my-apps', loadChildren: './my-apps/my-apps.module#MyAppsPageModule' },
  { path: 'cash-receipt', loadChildren: './cash-receipt/cash-receipt.module#CashReceiptPageModule' },
  { path: 'hr-options', loadChildren: './hr-options/hr-options.module#HrOptionsPageModule' },
  { path: 'main-landing', loadChildren: './main-landing/main-landing.module#MainLandingPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
