import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: '../tab1/tab1.module#Tab1PageModule'
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: '../tab2/tab2.module#Tab2PageModule'
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: '../tab3/tab3.module#Tab3PageModule'
          }
        ]
      },
      {
        path: 'summary',
        children: [
          {
            path: '',
            loadChildren: '../summary/summary.module#SummaryPageModule'
          }
        ]
      },
      {
        path: 'signature',
        children: [
          {
            path: '',
            loadChildren: '../signature/signature.module#SignaturePageModule'
          }
        ]
      },
      {
        path: 'ListPrePayments',
        children: [
          {
            path: '',
            loadChildren: '../list-pre-payments/list-pre-payments.module#ListPrePaymentsPageModule'
          }
        ]
      }
      // ,
      // {
      //   path: '',
      //   redirectTo: '/tabs/ListPrePayments',
      //   pathMatch: 'full'
      // }
    ]
  }
  ,
  {
    path: '',
    redirectTo: '/tabs/ListPrePayments',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
