import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';

import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import {AppModule} from '../app.module';
import { ToastController } from '@ionic/angular';

import {faEllipsisH} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-list-pre-payments',
  templateUrl: './list-pre-payments.page.html',
  styleUrls: ['./list-pre-payments.page.scss'],
})
export class ListPrePaymentsPage implements OnInit {

prepaymentList: any;
threeDotsIcon:any;
receiptInvoiceList: any = [{}];
index: number;
prepaymentExists: boolean = false;

  constructor(public router: Router, public api: RestApiService
      ,public loadingController: LoadingController
      , public appModule: AppModule
      , public toastController: ToastController
      ) { }

  ngOnInit() {
  }

  doReceipt2()
  {
// alert('jjj');
this.receiptInvoiceList=[{}];

this.index=0;
for(let x of this.prepaymentList){

  if (x.checked_invoice_flag == true)
  {
    console.log('it is checked **********'+x.voucher_num);
    // console.log('it is checked **********'+x.invoice_id);
     this.receiptInvoiceList[this.index]={invoice_id:x.invoice_id,
                                          invoice_num:x.invoice_num,
                                          voucher_num:x.voucher_num,
                                          employee_number:x.employee_number,
                                           employee_id: x.employee_id,
                                           org_id: x.org_id,
                                           invoice_type: x.invoice_type_lookup_code
                                        };
    //  this.receiptInvoiceList[this.index]=[{invoice_id, voucher_num}];
    this.index=this.index+1;
  }
}

if (this.index == 0)
{
 alert('Select Invoice');
 return;
}
console.log('local');
for(let x of this.receiptInvoiceList){
  //console.log('xxx//////////'+x.voucher_num);
  console.log('test value:'+x.voucher_num);
}
// AppModule.receiptInvoiceList=null;
AppModule.receiptInvoiceList=this.receiptInvoiceList;

console.log('appmodule');
for(let x of AppModule.receiptInvoiceList){
  console.log('xx value:'+x.voucher_num);
}

// alert('x last');
this.router.navigateByUrl('/signature');

  }
//   doReceipt()
//   {
// console.log('do cash receipt x1');
// // alert('s');
//
// this.index=0;
// for(let x of this.prepaymentList){
//
//   if (x.checked_invoice_flag == true)
//   {
//     // console.log('it is checked **********'+x.voucher_num);
//     // console.log('it is checked **********'+x.invoice_id);
//      this.receiptInvoiceList[this.index]={invoice_id:x.invoice_id,voucher_num:x.voucher_num};
//     //  this.receiptInvoiceList[this.index]=[{invoice_id, voucher_num}];
//     this.index=this.index+1;
//   }
// }
//
// for(let x of this.receiptInvoiceList){
//   //console.log('xxx//////////'+x.voucher_num);
//   console.log(x.voucher_num);
// }
//
// AppModule.receiptInvoiceList=this.receiptInvoiceList;
// this.router.navigateByUrl('/signature');
//   }


  ionViewWillEnter(){
    console.log('HomePage: ionViewWillEnter');
    this.threeDotsIcon=faEllipsisH;

    if (AppModule.personDetails[0].employee_number == null ||
    AppModule.personDetails[0].employee_number == undefined)
     return;

  this.getPrepaymentList();
  }

  async getPrepaymentList() {
    // console.log('employeeNumber:'+AppModule.personDetails[0].employee_number);
    //
    // if (AppModule.personDetails[0].employee_number == null)
    //    this.presentToast('Enter Employee Number.');

    const loading = await this.loadingController.create({
      message:'Pulling Prepayment List...'    });
    await loading.present();
    //await this.apix.getClassroom()


    await this.api.getPrepaymentList(AppModule.personDetails[0].employee_number+"") //AppModule.personDetails[0].employee_number+""
    .subscribe(res => {
        console.log('before list');
        console.log(res);
        console.log('after listsd');

        this.prepaymentList = res;
        if (res[0] == null)
        {
          console.log('no prepayments');
          this.prepaymentExists=false;
          // this.presentToast('No invoice exists..');
        }
             else
             {
              this.prepaymentList = res;
              console.log('there is data');
              this.prepaymentExists=true;
             }

        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
   }

}
