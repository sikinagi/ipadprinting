import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionPagePage } from './action-page.page';

describe('ActionPagePage', () => {
  let component: ActionPagePage;
  let fixture: ComponentFixture<ActionPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
