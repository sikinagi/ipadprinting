import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AlertController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import {AppModule} from '../app.module';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-action-page',
  templateUrl: './action-page.page.html',
  styleUrls: ['./action-page.page.scss'],
})
export class ActionPagePage implements OnInit {

  param1: any;
  companySegs: any = '';
  travel: any = '';
  salUpdFlag: any = '';
  apiStatus: any = "-1";
  letterType: any;
  attendanceFromDate: Date;
  attendanceToDate: Date;

  constructor(public router: Router, public alertController: AlertController
  ,public api: RestApiService, public loadingController: LoadingController) {}

  ngOnInit() {
  }

ionViewWillEnter(){
  // alert('xx');
  this.letterType=AppModule.letterType;
}

  submitRequest()
  {
    console.log('submitRequest');
    console.log('param1:'+this.param1);

    if (AppModule.letterType == 'Salary Certificate - English')
       this.invokeSubmitSIT();
     else if (AppModule.letterType == 'Payslip Letter')
      alert(AppModule.letterType);
      else if (AppModule.letterType == 'Attendance Letter')
           {

             alert(this.attendanceFromDate);
           }
       // alert(AppModule.letterType);
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Do you want to continue with other service?',
      animated: true,
      // message: 'Message <strong>text</strong>!!!',
      buttons: [
        {
          text: 'Exit',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            // this.router.navigateByUrl("/list-pre-payments");
            this.router.navigateByUrl("/main-landing");

                    }
        }, {
          text: 'Continue',
          handler: () => {
            console.log('Confirm Okay');
            this.router.navigateByUrl("/service-page");
          }
        }
      ]
    });

    await alert.present();
  }

  async invokeSubmitSIT() {
console.log('inside async invokeSubmitSIT()');
   const loading = await this.loadingController.create({
     message:'Submitting SIT request...'    });
   await loading.present();
   //await this.apix.getClassroom()
   console.log("before invoking invokeSubmitSIT ");

   await this.api.submitRequestSIT(AppModule.personDetails[0].person_id+"" //'pEmployeeId' :pEmployeeId,
   ,AppModule.personDetails[0].business_group_id+"" //'pBusinessGroupId' :pBusinessGroupId,
   ,AppModule.letterType //'pLetterType' :pLetterType,
   ,this.companySegs //'pCompanySegs' :pCompanySegs,
   ,this.travel //'pTravel' :pTravel,
   ,this.salUpdFlag //'pSalUpdFlag' :pSalUpdFlag,
   ,AppModule.personDetails[0].user_id+""
   ,AppModule.personDetails[0].employee_number+""
 ) //this.tempEmpNummber+""
   .subscribe(res => {
       console.log('before site request');
       console.log(res);
       this.apiStatus = res[0].retStatus+"";

       loading.dismiss();
       if ("0" == res[0].retStatus+"")
       {
         console.log('success');
          //loading.dismiss();
          //alert('Request has been submitted successfully');
          alert(res[0].retMsg);
          // this.presentAlertStatus(res[0].retMsg);
          // this.presentAlert();
          this.presentAlertConfirm();
        }
       else
       {
         console.log('failure');
          //loading.dismiss();
          alert(res[0].retMsg);
          // this.presentAlertStatus(res[0].retMsg);
          // this.presentAlert();
        }
// loading.dismiss();
//         this.presentAlertConfirm();

       console.log('after site request');
       //this.prepaymentList = res;
       // if (res[0] == null)
       // {
       //   this.blankPage=true;
       //   console.log('no data');
       //   //this.presentToast('No invoice exists..');
       // }
        //     else
        //     {
        //      this.blankPage=false;
        //      this.employeeDetails = res;
        //      this.appModule.empDetails2 = res;
        //      AppModule.personDetails = res;
        // console.log('there is data');
        // //this.xx = res[0].emp_image;
        // console.log(this.appModule.empDetails2[0].full_name);
        // console.log(AppModule.personDetails[0].full_name);
        // console.log(AppModule.personDetails[0].user_id);
        // //console.log(this.appModule.empDetails[0].phone_number);
        // console.log('appModule variable printed');
        //
        //     }
       //blankPage

       //this.sendSMS();
     }, err => {
       console.log(err);
       loading.dismiss();
     });
  }

}
