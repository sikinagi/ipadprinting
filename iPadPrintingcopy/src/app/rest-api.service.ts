import { Injectable } from '@angular/core';

import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { forkJoin } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

//LP-DXB-TO-22054
const apiUrl = "http://api.zippopotam.us/";
const apiUrlCash = "http://10.10.131.34:7003/cash-receipt-mobile/resources/cashreceipt/createReceipt2";
// const apiUrlEmpDetails = "http://10.10.131.34:7003/cash-receipt-mobile/resources/cashreceipt/getPrepayments";
const apiUrlEmpDetails = "http://10.10.131.34:7003/cash-receipt-mobile/resources/cashreceipt/getEmployeeDetails";
// const apiUrlPrepayment = "http://10.10.131.34:7003/cash-receipt-mobile/resources/cashreceipt/getEmployeeDetails";
const apiUrlRequest = "http://10.10.131.34:7003/cash-receipt-mobile/resources/cashreceipt/submitRequest";
const apiUrlSMS = "http://10.10.131.34:7003/cash-receipt-mobile/resources/cashreceipt/sendSMS";
const apiUrlRequestSIT = "http://10.10.131.34:7003/cash-receipt-mobile/resources/cashreceipt/submitRequestSit";
const apiUrlPrepayment = "http://10.10.131.34:7003/cash-receipt-mobile/resources/cashreceipt/getPrepayments";


@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  constructor(private http: HttpClient) { }

  getData(): Observable<any> {
    let response1 = this.http.get(apiUrl+'US/00210');
    let response2= this.http.get(apiUrl+'IN/110001');
    let response3 = this.http.get(apiUrl+'BR/01000-000');
    let response4 = this.http.get(apiUrl+'FR/01000');
    return forkJoin([response1, response2, response3, response4]);
  }


  private handleError(error: HttpErrorResponse) {
    console.log('error',error);
    console.log('error.error',error.error);
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        'Backend returned code ${error.status}, ' +
        'body was: ${error.error}');
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  createReceipt(pUserId:string, pSign: any, pInvoiceData: any): Observable<any> {
    let vx;
  console.log('inside create Receipt api:');
    const httpOptionsxx = {
      headers: new HttpHeaders({
        'Content-Type':'application/json ; charset=UTF-8' ,
      'Accept':'application/json',
      'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
      'Access-Control-Allow-origin':'*',
      'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE=',
      'pUserId':pUserId,
      'pSignature':pSign
      })
    };

    let bodyInvoice = [];

console.log('rest iterate');

    for(let x of pInvoiceData){
        console.log('voucherNumber'+x.voucher_num);
        console.log('invoiceNum'+x.invoice_num);
        console.log('employee_number'+x.employee_number);
        console.log('invoice_id'+x.invoice_id);
        console.log('orgId'+x.org_id);
        console.log('invoiceType'+x.invoice_type);
        bodyInvoice.push({invoiceId:x.invoice_id
                ,invoiceNum:x.invoice_num
                ,voucherNum:x.voucher_num
                ,employeeNumber:x.employee_number
                ,employeeId:x.employee_id
                ,orgId:x.org_id
                ,invoiceType:x.invoice_type
                });
      }

    return this.http.post(apiUrlCash,bodyInvoice,httpOptionsxx).pipe(
     map(this.extractData),
      catchError(this.handleError));
  }

  getPrepaymentList(pEmployeeNumber: any): Observable<any> {
    let vx;
      const httpOptionsxx = {
      headers: new HttpHeaders({
      'Content-Type':'application/json ; charset=UTF-8' ,
      'Accept':'application/json',
      'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
      'Access-Control-Allow-origin':'*',
      'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE=',
      'pEmployeeNumber' :pEmployeeNumber
      })
    };
    return this.http.post(apiUrlPrepayment,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }

  getEmployeeDetails(pEmployeeNumber: any): Observable<any> {

    const httpOptionsxx = {
      headers: new HttpHeaders({
      'Content-Type':'application/json ; charset=UTF-8' ,
      'Accept':'application/json',
      'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
      'Access-Control-Allow-origin':'*',
      'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE=',
      'pEmployeeNumber' :pEmployeeNumber
      })
    };

    // alert('pEmployeeNumber:'+pEmployeeNumber);
    return this.http.post(apiUrlEmpDetails,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }

  submitRequest(pEmployeeName: any): Observable<any> {

    const httpOptionsxx = {
      headers: new HttpHeaders({
      'Content-Type':'application/json ; charset=UTF-8' ,
      'Accept':'application/json',
      'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
      'Access-Control-Allow-origin':'*',
      'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE=',
      'pEmployeeName' :pEmployeeName
      })
    };
    return this.http.post(apiUrlRequest,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }


//method to call web service to submit SIT request
  submitRequestSIT(pEmployeeId: any
  ,pBusinessGroupId: any
,pLetterType: any
,pCompanySegs: any
,pTravel: any
,pSalUpdFlag: any
,pUserId: any
,pEmployeeNumber: any): Observable<any> {

    const httpOptionsxx = {
      headers: new HttpHeaders({
      'Content-Type':'application/json ; charset=UTF-8' ,
      'Accept':'application/json',
      'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
      'Access-Control-Allow-origin':'*',
      'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE=',
      'pEmployeeId' :pEmployeeId,
      'pBusinessGroupId' :pBusinessGroupId,
      'pLetterType' :pLetterType,
      'pCompanySegs' :pCompanySegs,
      'pTravel' :pTravel,
      'pSalUpdFlag' :pSalUpdFlag,
      'pUserId' :pUserId,
      'pEmployeeNumber': pEmployeeNumber
      })
    };
    return this.http.post(apiUrlRequestSIT,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }

  sendSMS(pPhoneNumber: any, pMessage: any): Observable<any> {

    const httpOptionsxx = {
      headers: new HttpHeaders({
      'Content-Type':'application/json ; charset=UTF-8' ,
      'Accept':'application/json',
      'Access-Control-Allow-Headers':'Allow-Headers,Content-Type;Access-Control-Allow-origin;',
      'Access-Control-Allow-origin':'*',
      'Authorization':'Basic Y2FzaHJlY2VpcHQ6d2VsY29tZTE=',
      'pPhoneNumber' :pPhoneNumber,
      'pMessage' :pMessage,
      })
    };
    return this.http.post(apiUrlSMS,null,httpOptionsxx).pipe(
     map(this.extractData),
    //);
      catchError(this.handleError));
  }
}
