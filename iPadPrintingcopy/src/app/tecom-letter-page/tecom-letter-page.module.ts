import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TecomLetterPagePage } from './tecom-letter-page.page';

const routes: Routes = [
  {
    path: '',
    component: TecomLetterPagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TecomLetterPagePage]
})
export class TecomLetterPagePageModule {}
