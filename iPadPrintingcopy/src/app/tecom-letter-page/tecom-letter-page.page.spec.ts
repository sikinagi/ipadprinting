import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TecomLetterPagePage } from './tecom-letter-page.page';

describe('TecomLetterPagePage', () => {
  let component: TecomLetterPagePage;
  let fixture: ComponentFixture<TecomLetterPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TecomLetterPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TecomLetterPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
