import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ArabicLetterPagePage } from './arabic-letter-page.page';

const routes: Routes = [
  {
    path: '',
    component: ArabicLetterPagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ArabicLetterPagePage]
})
export class ArabicLetterPagePageModule {}
