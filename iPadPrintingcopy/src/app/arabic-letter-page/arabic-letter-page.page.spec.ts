import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArabicLetterPagePage } from './arabic-letter-page.page';

describe('ArabicLetterPagePage', () => {
  let component: ArabicLetterPagePage;
  let fixture: ComponentFixture<ArabicLetterPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArabicLetterPagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArabicLetterPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
