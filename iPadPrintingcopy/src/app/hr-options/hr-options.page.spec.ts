import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrOptionsPage } from './hr-options.page';

describe('HrOptionsPage', () => {
  let component: HrOptionsPage;
  let fixture: ComponentFixture<HrOptionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrOptionsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrOptionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
