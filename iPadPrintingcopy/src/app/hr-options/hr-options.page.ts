// import { Component, OnInit } from '@angular/core';
//
// @Component({
//   selector: 'app-hr-options',
//   templateUrl: './hr-options.page.html',
//   styleUrls: ['./hr-options.page.scss'],
// })
// export class HrOptionsPage implements OnInit {
//
//   constructor() { }
//
//   ngOnInit() {
//   }
//
// }


import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';
import {AppModule} from '../app.module';
import { ToastController } from '@ionic/angular';
import {faSearch} from '@fortawesome/free-solid-svg-icons';
import { DomSanitizer } from '@angular/platform-browser';
// import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
// import { BackgroundMode } from '@ionic-native/background-mode/ngx';
// import { Platform } from '@ionic/angular';
// import {faFilePowerpoint} from '@fortawesome/free-solid-svg-icons';

import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";

@Component({
  selector: 'app-hr-options',
  templateUrl: './hr-options.page.html',
  styleUrls: ['./hr-options.page.scss'],
})
export class HrOptionsPage implements OnInit {
  scannedData: {};
  parsedData: string;
  underScore1: number;
  subString1: string;
  underScore2: number;
  subString2: string;
  barcodeScannerOptions: BarcodeScannerOptions;


  faCoffee2: any;

  xx='R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==';
data1: any;
data2: any;
data3: any;
data4: any;
otpNumber: any;
prepaymentList: any;
employeeNumber: any;
blankPage: any;
employeeDetails: any;
employeeNumberTemp: number;
tempEmpNummber: number;
image:any='';
contructFlag: boolean = false;
printOptions: any;
payslipIcon: any;
constructor(public router: Router, public api: RestApiService
    ,public loadingController: LoadingController, public appModule: AppModule, public toastController: ToastController
    ,private barcodeScanner: BarcodeScanner
    ,private sanitized: DomSanitizer
    // ,private backgroundMode: BackgroundMode
    // ,private platform: Platform
    // ,public camera: Camera
    ) {
    // this.payslipIcon=faSearch;
      this.contructFlag=true;
      // AppModule.blankPage=true;
      console.log('in construtor page 1');
      this.employeeNumber = "002328";
      //alert ('x');
  this.faCoffee2 = faSearch;

//   this.platform.ready().then(() => {
//   this.setDisableScroll(true);
// }


  // document.addEventListener("deviceready", this.onDeviceReady, false);
    }

//   setDisableScroll(disable: boolean) : void {
// let scroll = this.content.getScrollElement();
// scroll.style.overflowY = disable ? 'hidden' : 'scroll';
// }

    test(){
    //this.submitRequest();
    this.sendSMS();
  }

  scanCodeTemp() {
    this.router.navigateByUrl('/my-apps');

  }

    scanCode() {
      this.barcodeScanner
        .scan({preferFrontCamera:true, showFlipCameraButton:true})
        .then(barcodeData => {

          this.scannedData = barcodeData;
          this.parsedData = barcodeData.text;

          this.underScore1 = this.parsedData.indexOf("_");
          this.subString1 = this.parsedData.substring(this.underScore1+1, this.parsedData.length);

          this.underScore2 = this.subString1.indexOf("_");
          this.subString2 = this.subString1.substring(this.underScore1+1, this.subString1.length);

          this.employeeNumber=this.subString2.substring(0, this.subString2.indexOf("_"))+"";
          this.tempEmpNummber=parseInt(this.employeeNumber);

          //this.getList();
          this.getEmployeeDetails();

       })
        .catch(err => {
          console.log("Error", err);
        });

      // this.getEmployeeDetails(); //temp

    }

    async presentToast(pMessage: string) {
      const toast = await this.toastController.create({
        message: pMessage,
        duration: 2000
      });
      toast.present();
    }

    itemSelected(pdata:any)
    {
    console.log(pdata.places['place name']);
    }

   openSignatureForm(pdata:any)
  {
    console.log('calling createSignatureForm:');
    //console.log(this.appModule.prepaymentData);
    this.appModule.prepaymentList = pdata; //this.prepaymentList;

console.log(pdata.invoice_id);
console.log('step 2:');
      this.prepaymentList=null;
      this.employeeNumber=null;
      this.router.navigateByUrl('/signature');
      console.log('step 3:');
  }

  openServices()
  {
    console.log('calling openServices 1:');
    console.log('otpNumber:'+this.otpNumber);

    if (this.otpNumber == null)
    {
      alert('kindly enter OTP');
      return;
    }

     if (this.otpNumber != this.appModule.otp)
     {
      alert('Invalid OTP');
      return;
    }

    console.log('this.employeeNumber:'+this.employeeNumber);
    this.tempEmpNummber= parseInt(this.employeeNumber)+0;
    console.log('this.tempEmpNummber:'+this.tempEmpNummber);
    //console.log('this.employeeNumber:'+this.employeeNumber);
    this.router.navigateByUrl('/service-page');
    console.log('calling openServices 2:');
  }

  ionViewDidEnter(){


    console.log('HomePage: ionViewDidEnter');

  //  AppModule.printOptions.prompt='prompt 1';
   //AppModule.printOptions[0].prompt='ddd';

    // if(!this.contructFlag)

    this.blankPage=true;

     // console.log('dfdfd:'+this.appModule.blankPage);
  }


  ngOnInit() {
    console.log('oninit in list page');
    //this.getData();
    //this.getList();
  }

  populateList()
  {
    this.prepaymentList=null;
    this.getList();
  }


  async getList() {
    console.log('employeeNumber:'+this.employeeNumber);

    if (this.employeeNumber == null)
     this.presentToast('Enter Employee Number.');

    const loading = await this.loadingController.create({
      message:'Pulling List...'    });
    await loading.present();
    //await this.apix.getClassroom()
    await this.api.getPrepaymentList(this.employeeNumber)
    .subscribe(res => {
        console.log('before list');
        console.log(res);
        console.log('after listsd');
        console.log(res.employee_number);
        this.prepaymentList = res;
        if (res[0] == null)
        {
          this.blankPage=true;
          console.log('no data');
          this.presentToast('No invoice exists..');
        }
             else
             {
              this.blankPage=false;
              this.employeeDetails = res;
         console.log('there is data');
             }
        //blankPage
        loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
   }

   async getEmployeeDetails() {
    //console.log('employeeNumber:'+this.employeeNumber);

    // if (this.employeeNumber == null)
    //  this.presentToast('Invalid Employee Number');
console.log('inside async getEmployeeDetails()');
    const loading = await this.loadingController.create({
      message:'Pulling Employee Details...'    });
    await loading.present();
    //await this.apix.getClassroom()
    console.log("before invoking ws: "+this.tempEmpNummber);
    console.log("before invoking ws: + "+this.tempEmpNummber+"");
    //alert('temp:'+this.tempEmpNummber);
    await this.api.getEmployeeDetails(this.tempEmpNummber+"") //this.tempEmpNummber+"" 4529
    .subscribe(res => {
        console.log('before list');
        console.log(res);
        console.log('after list');
        this.prepaymentList = res;
        if (res[0] == null)
        {
          this.blankPage=true;
          console.log('no data');
          loading.dismiss();
          //this.presentToast('No invoice exists..');
        }
             else
             {
              this.blankPage=false;
              this.employeeDetails = res;
              this.appModule.empDetails2 = res;
              AppModule.personDetails = res;
         console.log('there is data');
         //this.xx = res[0].emp_image;
         console.log(this.appModule.empDetails2[0].full_name);
         console.log(AppModule.personDetails[0].full_name);
         console.log(AppModule.personDetails[0].user_id);
         //console.log(this.appModule.empDetails[0].phone_number);
         console.log('appModule variable printed');
         loading.dismiss();
         this.sendSMS();

             }
        //blankPage
        //loading.dismiss();
        //this.sendSMS();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
   }


   async sendSMS() {
console.log('inside async sendSMS()');

console.log(this.appModule.empDetails2[0].phone_number);

 if (this.appModule.empDetails2[0].phone_number == null)
 {alert('phone is null');
 return;
 }

    const loading = await this.loadingController.create({
      message:'sending OTP...'    });
    await loading.present();
    console.log('phone number printing-');

    console.log('.............');
    var otp = Math.random();
    var otpString = otp+"";
    this.appModule.otp = otpString.substring(3,7);
    console.log('otp:'+this.appModule.otp);
    await this.api.sendSMS(this.appModule.empDetails2[0].phone_number+"", this.appModule.otp+"") //this.appModule.empDetails2[0].phone_number+""
    .subscribe(res => {
        console.log('before sms');
        console.log(res);
        console.log('after sms');
        //this.prepaymentList = res;
        if (res[0] == null)
        {

          console.log('no data');
          loading.dismiss();
        }
             else
             {

         console.log('there is sms');
         console.log(res);
         loading.dismiss();

             }
      }, err => {
        console.log(err);
        loading.dismiss();
      });
   }

   ////Example for reference
  async getData() {
    console.log('inside getData');
    const loading = await this.loadingController.create({
      message: 'Loading'
    });
    await loading.present();
    this.api.getData()
      .subscribe(res => {
        console.log(res);
        this.data1 = res[0];
        this.data2 = res[1];
        this.data3 = res[2];
        this.data4 = res[3];
        loading.dismiss();
// console.log(this.data1);
// console.log(this.data2);
// console.log(this.data3);
console.log('************');
console.log(this.data4);
console.log(this.data4.places);

      }, err => {
        console.log(err);
        loading.dismiss();
      });
  }

  scanID(){
    this.blankPage=false;
    this.router.navigateByUrl('/service-page');

    // const options: CameraOptions = {
    //   quality: 100,
    //   cameraDirection:1,
    //   destinationType: this.camera.DestinationType.DATA_URL
    //   // encodingType: this.camera.EncodingType.JPEG,
    //   // mediaType: this.camera.MediaType.PICTURE
    // }

    // this.camera.getPicture(options).then((imageData) => {
    //  // imageData is either a base64 encoded string or a file URI
    //  // If it's base64 (DATA_URL):
    //  //alert(imageData)
    //  console.log('camera output:'+imageData);
    //  alert('a1: '+imageData);
    //  this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
    //  alert('a1: '+this.image);
    // }, (err) => {
    //   alert('my error');
    //  // Handle error
    //  alert("error "+JSON.stringify(err))
    // });

  }



}
