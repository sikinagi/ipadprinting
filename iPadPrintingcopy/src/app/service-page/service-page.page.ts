import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import {AppModule} from '../app.module';
import { RestApiService } from '../rest-api.service';
import { LoadingController } from '@ionic/angular';
import {Router} from '@angular/router';
// import {faFilePowerpoint} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-service-page',
  templateUrl: './service-page.page.html',
  styleUrls: ['./service-page.page.scss'],
})
export class ServicePagePage implements OnInit {

payslipIcon: any;
appName: string;
  constructor(public toastController: ToastController, public appModule: AppModule, public api: RestApiService
    , public loadingController: LoadingController, public router: Router) {
// this.payslipIcon=faFilePowerpoint;
    }

    ionViewWillEnter(){
      console.log('HomePage: ionViewWillEnter');
    this.appName=AppModule.appName;
    }

  ngOnInit() {
  }

  openCashAcknowledge()
  {

    // this.router.navigateByUrl('/cash-receipt');
    console.log('pre 1');
    this.router.navigateByUrl('/list-pre-payments');
  }

  printIncrementLetter()
  {
    console.log('calling printIncrementLetter');
    this.presentToast('Printing Increment letter ....');
  }

  printSalarySlip(t)
  {
    console.log('calling printSalarySlip:  '+t);
    // this.presentToast('Printing Salary Certificate ....');
    // this.submitRequest();
    AppModule.letterType='Payslip Letter';
    this.router.navigateByUrl("/action-page");
  }

  goAction(t)
  {
    console.log('calling goAction:  '+t);
    // this.presentToast('Printing Salary Certificate ....');
    // this.submitRequest();
    AppModule.letterType=t;
    this.router.navigateByUrl("/action-page");
  }


  // attendanceLetter(t)
  // {
  //   console.log('calling attendanceLetter:  '+t);
  //   // this.presentToast('Printing Salary Certificate ....');
  //   // this.submitRequest();
  //   AppModule.letterType=t;
  //   this.router.navigateByUrl("/action-page");
  // }
  async submitRequest() {
    //console.log('employeeNumber:'+this.employeeNumber);

    // if (this.employeeNumber == null)
    //  this.presentToast('Invalid Employee Number');
console.log('inside async submitRequest()');
console.log(this.appModule.empDetails2[0].full_name);

    const loading = await this.loadingController.create({
      message:'Submitting requests...'    });
    await loading.present();
    await this.api.submitRequest(this.appModule.empDetails2[0].full_name)
    .subscribe(res => {
        console.log('before submit');
        console.log(res);
        console.log('after submit');
        //this.prepaymentList = res;
        if (res[0] == null)
        {
          //this.blankPage=true;
          console.log('no data');
          //this.presentToast('No invoice exists..');
          loading.dismiss();
        }
             else
             {
              //this.blankPage=false;
              //this.employeeDetails = res;
         console.log('there is data');
         loading.dismiss();
         //alert('result');
         alert(res[0].retMsg);
         this.appModule.requestDetails = res;
         console.log('variable printing');
         console.log(this.appModule.requestDetails);
         console.log('appModule variable printed');
             }
        //blankPage
        //loading.dismiss();
      }, err => {
        console.log(err);
        loading.dismiss();
      });
   }

  async presentToast(pMessage: string) {
    const toast = await this.toastController.create({
      message: pMessage,
      duration: 2000
    });
    toast.present();
  }

  showEnglishLetters()
  {
    console.log('Show english letters');
    this.router.navigateByUrl('/english-letter-page');
  }

  showArabicLetters()
  {
    console.log('Show arabic letters');
    this.router.navigateByUrl('/arabic-letter-page');
  }

  showTecomLetters()
  {
    console.log('Show tecom letters');
    this.router.navigateByUrl('/tecom-letter-page');
  }

}
