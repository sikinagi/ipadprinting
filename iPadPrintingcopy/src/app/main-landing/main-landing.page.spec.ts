import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainLandingPage } from './main-landing.page';

describe('MainLandingPage', () => {
  let component: MainLandingPage;
  let fixture: ComponentFixture<MainLandingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainLandingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainLandingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
