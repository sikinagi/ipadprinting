
import { Component, OnInit, ViewChild } from '@angular/core';
import {SignaturePad} from 'angular2-signaturepad/signature-pad';
import { removeDebugNodeFromIndex } from '@angular/core/src/debug/debug_node';
import { R3TargetBinder } from '@angular/compiler';

import { LoadingController } from '@ionic/angular';
import { RestApiService } from '../rest-api.service';

import {Router} from '@angular/router';

import {AppModule} from '../app.module';
import { AlertController } from '@ionic/angular';

import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-signature',
  templateUrl: './signature.page.html',
  styleUrls: ['./signature.page.scss'],
})
export class SignaturePage implements OnInit {
  @ViewChild(SignaturePad) public signaturePad : SignaturePad;
  @ViewChild('xxt1') xxt1:any;
  @ViewChild('xxt2') xxt2:any;

  imgUrl="../assets/img/sign.jpg";

  public signaturePadOptions : Object = {
      'minWidth': 2,
      'canvasWidth': 800,
      'canvasHeight': 420,
      'backgroundColor': '#eff1f6',
      'penColor': 'red'
    };

data1: any;
data2: any;
prepaymentList: any;
invoiceId: any;
signUrl: any;

prepaymentDetails: any;
  constructor(public api: RestApiService, public loadingController: LoadingController,
    public router: Router, public appModule: AppModule, public alertController: AlertController,
    public toastController: ToastController) {
   console.log('signature constructor');
    }

    ionViewWillEnter(){
      console.log('HomePage: ionViewWillEnter');
      this.prepaymentDetails = AppModule.prepaymentDetails;

      for(let x of AppModule.receiptInvoiceList){
          console.log('tttttt//////////'+x.voucher_num);
          console.log('tttttt//////////'+x.invoice_num);
        }
      }

   drawComplete() {
     console.log('image url: '+ this.signaturePad.toDataURL());
     // this.createReceipt();

     console.log('inside clear'+this.signaturePad.toDataURL());
     let c:any;


    c=this.xxt1.nativeElement;
     let ctx = c.getContext("2d");
let img: any;

img=this.xxt2.nativeElement;
img.src =  this.signaturePad.toDataURL();
let m=this;
setTimeout(function(){
ctx.drawImage(img,0,0);
ctx.strokeStyle = "#fff";
ctx.font = "30px Arial";

 ctx.strokeText("Name: "+AppModule.personDetails[0].full_name,10,445);

 let datex =new Date();
 let formattedDate = datex.getDate() + "-" + datex.toLocaleString('default', { month: 'short' }) + "-" + datex.getFullYear();

 ctx.strokeText("Date: "+formattedDate,10,480);
// alert('xxx'+AppModule.personDetails[0].user_id);
// AppModule.personDetails[0].full_name

console.log(c.toDataURL())
m.signUrl = c.toDataURL();
m.createReceipt();
}, 1);



   }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Receipt has been created.',
      duration: 2000
    });
    toast.present();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Error',
      message: 'Unable to submit request',
      buttons: ['OK']
    });

    await alert.present();
  }

   drawClear() {
      this.signaturePad.clear();
      console.log('inside clear');

      console.log('create receipt...:');

      console.log('print done');

  }

   drawCancel() {
    console.log('you are in drawCancel');
    this.signaturePad.clear();

     this.router.navigateByUrl('/list-pre-payments');

  }

  ngOnInit() {
    console.log('inside ngOnInit');

  }


async createReceipt() {
  console.log('pushing');

  const loading = await this.loadingController.create({
    message:'Submitting...'    });
  await loading.present();
  console.log('before sendig ws', this.signUrl);
  // await this.api.creteReceipt(this.signaturePad.toDataURL(), this.prepaymentDetails[0])
  await this.api.createReceipt(AppModule.personDetails[0].user_id+"", this.signUrl, AppModule.receiptInvoiceList)
  .subscribe(res => {
      console.log('befpr');
      console.log(res);
      console.log('after');
      this.data1 = res;
      loading.dismiss();

      this.presentToast();

      this.router.navigateByUrl('/list-pre-payments');

    }, err => {
      console.log('in error 1..');
      console.log(err);
      loading.dismiss();
      this.presentAlert();
    });
}

}
